package plugins.stef.library;

import icy.plugin.abstract_.Plugin;
import icy.plugin.interface_.PluginLibrary;

/**
 * Icy wrapper for the JogAmp JOAL library.
 * 
 * @author Stephane Dallongeville
 */
public class JOALPlugin extends Plugin implements PluginLibrary
{
    static
    {
        try
        {
            // load native library
            Plugin.loadLibrary(JOALPlugin.class, "joal");
        }
        catch (Throwable t)
        {
            System.err.println("Could not load joal native library.");
            System.err.println(t.getMessage());
        }
    }
}
